package ifd.sparing.POJOs;

public class UserPOJO {
    private String username,password,namaLengkap,phoneNumber;

    public UserPOJO(String username, String password, String namaLengkap, String phoneNumber) {
        this.username = username;
        this.password = password;
        this.namaLengkap = namaLengkap;
        this.phoneNumber = phoneNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
