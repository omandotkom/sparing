package ifd.sparing;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.shrikanthravi.chatview.data.Message;
import com.shrikanthravi.chatview.widget.ChatView;

import java.util.Arrays;

public class MessageListActivityNew extends AppCompatActivity {
    private ChatView chatView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list_new);
        chatView = (ChatView) findViewById(R.id.chatView);
        Message message = new Message();
        message.setBody("Jadi main nggak nih ?"); //message body
        message.setType(Message.RightSimpleMessage); //message type
        message.setUserName("Ridwan Risandi"); //sender name
        //sender icon
        message.setUserIcon(null);
        chatView.addMessage(message);

//sample code to add message to left
        Message message1 = new Message();
        message1.setBody("boleeeh");
        message1.setType(Message.LeftSimpleMessage);
        message1.setUserName("Khalid Abdurrahman");
        message1.setUserIcon(null);
        message1.setTime("12:30");
        chatView.addMessage(message1);


        Message message2 = new Message();
        message2.setBody("Mayuh lah");
        message1.setTime("12:35");
        message2.setType(Message.LeftSimpleMessage);
        message2.setUserName("Hesa Abda");
        message2.setUserIcon(null);
        chatView.addMessage(message2);

        chatView.setOnClickSendButtonListener(new ChatView.OnClickSendButtonListener() {
            @Override
            public void onSendButtonClick(String s) {
                Message message1 = new Message();
                message1.setBody(s);
                message1.setType(Message.RightSimpleMessage);
                message1.setUserName("Ridwan Risandi");
                message1.setUserIcon(null);
                message1.setTime("12:40");
                chatView.addMessage(message1);

            }
        });
       }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
