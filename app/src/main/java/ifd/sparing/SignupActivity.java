package ifd.sparing;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import ifd.sparing.POJOs.UserPOJO;


public class SignupActivity extends AppCompatActivity {
private TextView inputName,inputEmail, inputPassword, inputPhone;
private final String LOGCAT="SINGUP";
private FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        inputName = findViewById(R.id.input_name);
        inputEmail = findViewById(R.id.input_email);
        inputPassword = findViewById(R.id.input_password);
        inputPhone = findViewById(R.id.input_phone);

    }
    public void registerAccount(View view){
        //UserPOJO user = new UserPOJO(inputName.getText().toString(),inputPassword.getText().toString(),inputName.getText().toString(),inputPhone.getText().toString());
        Map<String,String> user = new HashMap<>();
        user.put("username",inputEmail.getText().toString());
        user.put("password",inputPassword.getText().toString());
        user.put("email",inputEmail.getText().toString());
        user.put("phone",inputPhone.getText().toString());

        db.collection("users")
                .add(user)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(getApplicationContext(),"Berhasil mendaftarakan akun",Toast.LENGTH_LONG).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(),"Gagal mendaftarakan akun",Toast.LENGTH_LONG).show();
            }
        });

    }
private boolean validate(){
        //tulis kode validasi di sini
        return true;
}
}
