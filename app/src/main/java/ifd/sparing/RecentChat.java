package ifd.sparing;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import ifd.sparing.adapters.GroupListAdapter;
import ifd.sparing.model.Group;

public class RecentChat extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_chat);
        RecyclerView rvRecentChat = (RecyclerView) findViewById(R.id.rvRecentChat);

        ArrayList<Group> groupsList = new ArrayList<Group>();

        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));
        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));

        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));

        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));

        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));

        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));

        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));

        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));

        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));
        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));
        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));
        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));
        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));
        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));
        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));
        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));
        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));
        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));
        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));
        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));


        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));

        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));
        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));


        groupsList.add(new Group("14:04", "Badminton - Khalid", "pada mau ketemu dimana ?"));


        GroupListAdapter adapter = new GroupListAdapter(groupsList, getBaseContext());
        // Attach the adapter to the recyclerview to populate items
        rvRecentChat.setAdapter(adapter);
        // Set layout manager to position the items
        rvRecentChat.setLayoutManager(new LinearLayoutManager(this));
        // That's all!


    }

}
