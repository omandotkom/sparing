package ifd.sparing.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ifd.sparing.MessageListActivityNew;
import ifd.sparing.R;
import ifd.sparing.model.Sparing;


public class ThreadListAdapter extends RecyclerView.Adapter<ThreadListAdapter.ViewHolder> {
    private List<Sparing> sparingList;
    private Context ctx;
    public ThreadListAdapter(List<Sparing> grp,Context c){
        this.sparingList = grp;this.ctx = c;
    }
    @NonNull
    @Override
    public ThreadListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View inviteView = inflater.inflate(R.layout.item_sparinglist, viewGroup, false);

        // Return a new holder instance
        ThreadListAdapter.ViewHolder viewHolder = new ThreadListAdapter.ViewHolder(inviteView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ThreadListAdapter.ViewHolder holder, int i) {
        Sparing sparing = sparingList.get(i);

        // Set item views based on your views and data model
        TextView title = holder.namaOlahraga;
        title.setText(sparing.getmOlahraga());

        TextView tanggal = holder.tanggalOlahraga;
        tanggal.setText(sparing.getmTanggal());

        TextView jam = holder.jamOlahraga;
        jam.setText(sparing.getJam());

        TextView kapasitas = holder.kapasitas;
        kapasitas.setText(sparing.getMcurrentPerson() + "/" + sparing.getmTotalAnggota());

    }


    @Override
    public int getItemCount() {
        return sparingList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
    private TextView namaOlahraga, tanggalOlahraga,jamOlahraga, kapasitas;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            namaOlahraga= (TextView) itemView.findViewById(R.id.nmOlahraga);
            tanggalOlahraga = (TextView) itemView.findViewById(R.id.tanggal);
            kapasitas = (TextView) itemView.findViewById(R.id.kapasitas);
            jamOlahraga = (TextView) itemView.findViewById(R.id.lblJam);
            ConstraintLayout c = (ConstraintLayout) itemView.findViewById(R.id.dalem);
            c.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ctx.startActivity(new Intent(ctx,MessageListActivityNew.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            });
        }
    }
}
