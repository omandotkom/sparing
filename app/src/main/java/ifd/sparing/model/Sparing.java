package ifd.sparing.model;

public class Sparing {
    private String mOlahraga;
    private Integer mTotalAnggota;
    private Integer mcurrentPerson;
    private String mTanggal;
    private String jam;
    public Sparing(String mOlahraga, Integer mTotalAnggota, Integer mcurrentPerson, String mTanggal, String jam) {
        this.mOlahraga = mOlahraga;
        this.mTotalAnggota = mTotalAnggota;
        this.mcurrentPerson = mcurrentPerson;
        this.mTanggal = mTanggal;
        this.jam = jam;
    }

    public String getJam() {
        return jam;
    }

    public Integer getmTotalAnggota() {
        return mTotalAnggota;
    }

    public Integer getMcurrentPerson() {
        return mcurrentPerson;
    }

    public String getmOlahraga() {
        return mOlahraga;
    }

    public void setmOlahraga(String mOlahraga) {
        this.mOlahraga = mOlahraga;
    }


    public String getmTanggal() {
        return mTanggal;
    }

    public void setmTanggal(String mTanggal) {
        this.mTanggal = mTanggal;
    }
}
