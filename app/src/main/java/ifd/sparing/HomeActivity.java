package ifd.sparing;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import ifd.sparing.adapters.ThreadListAdapter;
import ifd.sparing.model.Sparing;

public class HomeActivity extends AppCompatActivity

        implements NavigationView.OnNavigationItemSelectedListener {

    private ImageView imgProfile;
private ArrayList<Sparing> sparingList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView badminton = (ImageView) findViewById(R.id.btn_bulutangkis);
        badminton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this,BuatThread.class));
            }
        });
        toolbar.setNavigationIcon(R.drawable.if_talk_chat_message);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
               startActivity(new Intent(HomeActivity.this,RecentChat.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                return true;
            }
        });
        RecyclerView rvSparing = (RecyclerView) findViewById(R.id.rvSparing);

        sparingList = new ArrayList<Sparing>();

        sparingList.add(new Sparing("Badminton",4,2,"15 April 2018","15:00 WiB"));

        sparingList.add(new Sparing("Badminton",4,2,"15 April 2018","15:00 WiB"));

        sparingList.add(new Sparing("Badminton",4,2,"15 April 2018","15:00 WiB"));

        sparingList.add(new Sparing("Badminton",4,2,"15 April 2018","15:00 WiB"));



        sparingList.add(new Sparing("Badminton",4,2,"15 April 2018","15:00 WiB"));

        sparingList.add(new Sparing("Badminton",4,2,"15 April 2018","15:00 WiB"));

        sparingList.add(new Sparing("Badminton",4,2,"15 April 2018","15:00 WiB"));

        sparingList.add(new Sparing("Badminton",4,2,"15 April 2018","15:00 WiB"));

        sparingList.add(new Sparing("Badminton",4,2,"15 April 2018","15:00 WiB"));

        sparingList.add(new Sparing("Badminton",4,2,"15 April 2018","15:00 WiB"));

        sparingList.add(new Sparing("Badminton",4,2,"15 April 2018","15:00 WiB"));

        ThreadListAdapter adapter = new ThreadListAdapter(sparingList,getBaseContext());
        rvSparing.setAdapter(adapter);
        rvSparing.setLayoutManager(new LinearLayoutManager(this));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        LinearLayout linearLayout = (LinearLayout)  headerView.findViewById(R.id.lnr);
        ImageView profilePic = (ImageView) linearLayout.findViewById(R.id.imageProfile);
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_futsal) {
            // Handle the camera action
            Toast.makeText(getBaseContext(),"Saat ini baru Bulutangkis yang di dukung",Toast.LENGTH_LONG).show();
        }
        else if (id == R.id.nav_sepakbola) {
            Toast.makeText(getBaseContext(),"Saat ini baru Bulutangkis yang di dukung",Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_bulutangkis) {
            startActivity(new Intent(HomeActivity.this,BuatThread.class));

        } else if (id == R.id.nav_bolavoli) {
            Toast.makeText(getBaseContext(),"Saat ini baru Bulutangkis yang di dukung",Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_bolabasket) {
            Toast.makeText(getBaseContext(),"Saat ini baru Bulutangkis yang di dukung",Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_bolatenis) {
            Toast.makeText(getBaseContext(),"Saat ini baru Bulutangkis yang di dukung",Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_tenismeja) {
            Toast.makeText(getBaseContext(),"Saat ini baru Bulutangkis yang di dukung",Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_lari) {
            Toast.makeText(getBaseContext(),"Saat ini baru Bulutangkis yang di dukung",Toast.LENGTH_LONG).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
